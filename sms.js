const express = require('express');
const router = express.Router();

// Get authentication secrets from a file
const credentials = require('./cert');

const AfricasTalking = require('africastalking')(credentials.TEST_ACCOUNT);
const sms = AfricasTalking.SMS;

// Send SMS route
router.post('/send', (req, res) => {
    const options = {
        to: req.body.to,
        message: req.body.message,
        from: req.body.from,
        enque: true
    }
    sms.send(options)
        .then(response => {
            res.json(response);
        })
        .catch(error => {
            res.json(error.toString());
        });
});

module.exports = router;
