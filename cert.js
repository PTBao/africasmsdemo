'use strict';

exports.TEST_ACCOUNT = {
    apiKey: process.env.APIKEY,
    username: process.env.USERNAME,
    format: "json"
}