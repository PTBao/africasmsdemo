#### TransUnion Kenya



1. **Comprehensive Consumer Report with Positive Score**:

- **Libary:** soap

- **Function name:** getProduct109

- **Argument Request:**

  - *Mandatory*:

    - username: String
    - password: String
    - code: String
    - infinityCode: String
    - reportSector: Int (1 or 2)
    - reportReason Int (1 -> 12)

  - *Minimum 2*: At least 2 variables must have value

    - name1: String
    - name2: String
    - name3: String
    - name4: String

  - *Minimum 1*: At least 1 variables must have value

    - nationalID: String
    - passportNo: String
    - serviceID: String
    - alienID: String
    - taxID: String

  - *Optional*: dateOfBirth, postalBoxNo, postalBoxNo, postalCountry, telephoneWork

    ,telephoneHome, telephoneMobile, physicalAddress, physicalTown, physicalCountry

- Example Code:

  - Request:

  ```nodejs
  var soap = require('soap');
  var url = ${url};
  var userNmTran = ${userNmTran} // Username of Transport Level
  var passwordTran = ${passwordTran} // Password of Transport Level
  var auth = "Basic " + new Buffer(userNmTran + ":" + passwordTran).toString("base64");
  
  soap.createClient(url, { wsdl_headers: {Authorization: auth} }, (err, client) => {
      if(err){
          console.log(err);
      }
      client.setSecurity(new soap.BasicAuthSecurity(userNmTran, passwordTran));
      const args = {
          username: ${userNmMess}, // Username of Message Level
          password: ${PasswordMess}, // Password of Message Level
          code: "2472",
          infinityCode: "ke123456789",
          name1: "SurMobTestE",
          name2: "OthernameMobtestE",
          name3: "",
          name4: "",
          nationalID: "22053498",
          serviceID: "",
          alienID: "",
          taxID: "",
          reportSector: 1,
          reportReason: 2
          };
      // console.log(client.describe())
      client.getProduct109(args, (err, res) => {
          if (err) {
              console.log("error: ")
              console.error(err);
          }else{
              console.log("body: ")
              console.log(res.return.accountList);
              console.log(res.return.personalProfile);
              console.log(res.return.scoreOutput);
          }
         
      })
  });
  ```

  - Response: Console Log

  ```
  body: 
  // accountList
  [
    {
      accountNo: 'FASFWE14213',
      accountOpeningDate: 2019-09-19T07:00:00.000Z,
      accountStatus: 'PERFORMING',
      accountType: 'M-SHWARI LOAN',
      arrearAmount: 0,
      arrearDays: 0,
      balanceAmount: 1200,
      currency: 'KES',
      disputed: 'false',
      lastPaymentDate: 2019-10-22T07:00:00.000Z,
      principalAmount: 2400,
      repaymentTerm: 'MONTHLY',
      scheduledPaymentAmount: 0,
      worstArrear: 0
    }
  ]
  // personalProfile
  {
    crn: 32288510,
    dateOfBirth: 1969-01-27T05:00:00.000Z,
    fullName: 'SurMobTestE OthernameMobtestE',
    gender: 'F',
    nationalID: '22053498',
    otherNames: 'OthernameMobtestE',
    salutation: 'Ms',
    surname: 'SurMobTestE'
  }
  // scoreOutput
  {
    grade: 'II',
    positiveScore: '524',
    probability: '41.82',
    reasonCodeAARC1: 'Average credit limit for loans is relatively low',
    reasonCodeAARC2: 'Loan installments are not being  paid on time',
    reasonCodeAARC3: 'Borrowed a lot on loans other than secured loans, those with regular payments or those where balances do not have to be paid every month',
    reasonCodeAARC4: 'Many loan applications in the recent past (for non mobile loans)'
  }
  ```

- Generic Score Grade:

  ![Screen Shot 2020-08-06 at 3.16.40 PM](https://imageptbao.s3-ap-southeast-1.amazonaws.com/Screen+Shot+2020-08-06+at+3.16.40+PM.png)

2. **Test function with Postman**:

- *Method*: Post

- *URL*: ${URL}

- *Authorization*: 

  - Type: Basic Auth
  - Username: **Username of Transport Level**
  - Password:  **Password of Transport Level**

- *Header*:

  - Content-type: text/xml (Manual add header)
  - ....

- *Body*: 

  - Update function
  - Update argument param

  ~~~
  <?xml version="1.0" encoding="utf-8"?>
  <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:tns="http://ws.crbws.transunion.ke.co/">
      <soap:Header></soap:Header>
      <soap:Body>
          <tns:getProduct109>
              <username>${userNmMess}</username>
              <password>${passwordMess}</password>
              <code>2472</code>
              <infinityCode>ke123456789</infinityCode>
              <name1>SurMobTestE</name1>
              <name2>OthernameMobtestE</name2>
              <name3></name3>
              <name4></name4>
              <nationalID>22053498</nationalID>
              <serviceID></serviceID>
              <alienID></alienID>
              <taxID></taxID>
              <reportSector>2</reportSector>
              <reportReason>3</reportReason>
          </tns:getProduct109>
      </soap:Body>
  </soap:Envelope>
  ~~~

   

3. **HTTP Response Status**:

- **101**: General Authentication Error
- **102**: Invalid Infinity Code 
- **103**: Invalid Authentication Credentials 
- **104**: Password expired 
- **106**: Access Denied 
- **109**: Account locked 
- **200**: Product request processed successfully 
- **202**: Credit Reference Number not found 
- **203**: Multiple Credit Reference Number Found 
- **204**: Invalid report reason 
- **209**: Invalid Sector ID 
- **301**: Insufficient Credit 
- **402**: Required input missing 
- **403**: General Application Error 
- **404**: Service temporarily unavailable 
- **408**: Unable to verify National ID
- **603**: Product request processed successfully but Pdf not generated.
