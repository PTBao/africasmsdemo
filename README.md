#### **#AfricaSMSDemo**

**1. Send SMS**

- **Emulator Test**: From the dashboard select Launch Simulator for emulator phone number

- **Library**: body-parser, express, africastalking

- **SMS Service(africastalking)**: send({ to, from, message, enqueue }): Send a message

  - *Params*:

    - **to**: Recipients phone number. REQUIRED
    - **from**: Short code or alphanumeric ID that is registered with Africa's Talking account
    - **message**: SMS content. REQUIRED
    - **enqueue**: Set to true if you would like to deliver as many messages to the API without waiting for an acknowledgement from telcos

  - *Response*:

    - **Message**: A summary of the total number and the total cost incurred.
    - **Recipients(List)**:
      - Status Code: This corresponds to the status of the request
        - 100: Processed
        - 101: Sent
        - 401: RiskHold
        - 402: InvalidSenderId
        - 403: InvalidPhoneNumber
        - 404: UnsupportedNumberType
        -  405: InsufficientBalance
        - 406: UserInBlacklist
        - 407: CouldNotRoute
        - 500: InternalServerError
        - 501: GatewayError
        - 502: RejectedByGateway
    - **number**: The phone number
    - **cost**: Amount incurred to send this sms
    - **status**: Success or Failed
    - **messageId**: The Message Id received when the sms was sent.

  - *Example*:

    - **Single SMS**:

      - Request:

        ```
        {
           "to": "+841679608838",
           "message": "Test Africa Talking",
           "from":"8838"
        }
        ```

      

      - Response:

        ```
        {
          "SMSMessageData": {
             "Message": "Sent to 1/1 Total Cost: KES 2.0231",
             "Recipients": [
                 {
                    "statusCode": 101,
                    "number": "+841679608838",
                    "cost": "USD 0.0200",
                    "status": "Success",
                    "messageId": "ATXid_749dbaeb2696baf82fe9d109aad38a43"
                  }
              ]
           }
        }
        ```

        

    - **Multi SMS**:

      - Request:

        ```
        {
          "to": ["+841679608838","+841679991234","+841679608838"],
          "message": "Test Africa Talking1",
          "from":"8838"
        }
        ```

        

      - Response:

        ```
        {
           "SMSMessageData": {
                "Message": "Sent to 2/2 Total Cost: KES 4.0461",
                "Recipients": [
                    {
                        "statusCode": 101,
                        "number": "+841679991234",
                        "cost": "USD 0.0200",
                        "status": "Success",
                        "messageId": "ATXid_d5cbf4af794e53b8ed8a0d7089aa2c95"
                    },
                    {
                        "statusCode": 101,
                        "number": "+841679608838",
                        "cost": "USD 0.0200",
                        "status": "Success",
                        "messageId": "ATXid_117eeb0a0a885ff1725fdc4415c56bc5"
                    }
                ]
            }
        }
        ```

        



**2. Receive SMS**

- **Setup**: From the dashboard select SMS -> SMS Callback URLs -> Incoming Messages

- **Callback URL**: (method: POST)

- **Emulator Test**: Simulator a message send to Short Code or Alphanumeric ID(Branch Name)

- **Info response from Africa**:

  ```
  Body:linkId=5939927e-33a0-4dbd-9ba4-7c5f5f95b0cf
           &text=Hello+ITR
           &to=8838
           &id=57b1eb21-26cb-4132-924c-97b18a0680f3
           &date=2020-08-03+08%3A45%3A16
           &from=%2B841679608838
  ```

  

  - *linkId*	: Parameter required when responding to an on-demand user request with a premium message.
  - *text*	: The message content
  - *to*	: The number to which the message was send (Short Code or Alphanumerics)
  - *id*	: The internal ID that we use to store this message.
  - *date*	: The date and time when the message was received.
  - *from*	: The number that sent the message.

**3. Note**

- **Limit length a SMS**: A Standard SMS Message has a maximum of 160 characters. If exceeding 160 characters will constitute a ‘second’ message
- Environment: 
  - **APIKEY**: "53610e9dbcc5b19f57bd979c4141345698b44cb7a7d77213272454eabb260207"
  - **USERNAME**: "sandbox"

